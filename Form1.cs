﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Text.RegularExpressions;

namespace KRY_0x03
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		protected bool encr = true;
		protected int enc = 1;
		RandomNumberGenerator rng = new RNGCryptoServiceProvider();
		static int block_size = ((Convert.ToInt32(key_size.b1024) * 2) / 8) - 1;
		static int b1024 = Convert.ToInt32(key_size.b1024); //512b primes
		static int b2048 = Convert.ToInt32(key_size.b2048);
		static int b3072 = Convert.ToInt32(key_size.b3072);
		static int b4096 = Convert.ToInt32(key_size.b4096);
		static int randbytesize = 0;
		static byte[] partOfMessage = new byte[block_size];
		static BigInteger P = 0;
		static BigInteger Q = 0;
		static BigInteger E = 0;
		static BigInteger D = 0;
		static BigInteger N = 0;
		static BigInteger C = 0;
		static BigInteger M = 0;
		static bool isprime_p, isprime_q;
		static int candidates_tried;
		static DateTime gp_start, gp_finish;
		static int numThreads = 3;
		int custom_threads = 0;

		enum key_size: uint
		{
			b1024 = 512,
			b2048 = 1024,
			b3072 = 1536,
			b4096 = 2048,
		}

		public void gen_primes()
		{
			if (randbytesize == 0)
			{
				randbytesize = b1024; // default to something
			}

			int min_checks = mr_min_checks(randbytesize * 2);
			byte[] random = new byte[randbytesize];
			BigInteger rand_int = 0;
			candidates_tried = 0;
			isprime_p = false;
			isprime_q = false;

			while (!isprime_p || !isprime_q)
			{
				/* P(isprime(2^randbytesize)) == 2/(randbytesize*ln(2)) */
				candidates_tried++;
				rng.GetBytes(random);
				rand_int = new BigInteger(random);
				if (rand_int.IsEven || rand_int < 2) { continue; }
				if (!is_huge_prime(rand_int, min_checks))
				{
					Console.WriteLine($"[*] rand_int is not a_prime! (tried: {candidates_tried}:\n{rand_int})");
					continue;
				}
				Console.WriteLine($"[*] rand_int: {rand_int} \tis a prime!\n");
				if (P != 0)
				{
					if (rand_int != P)
					{
						Form qq = new Form1();
						lock (qq)
						{
							Q = rand_int;
							isprime_q = true;
						}
						Console.WriteLine("[*] found q!\n");
						break;
					}
					Console.WriteLine("\n[!] q cannot equal p");
					continue;
				}

				Form pp = new Form1();
				lock (pp)
				{
					P = rand_int;
					isprime_p = true;
				}
				Console.WriteLine("[*] found p!\n");
			}
		}

		static int mr_min_checks(int bits)
		{
			if (bits > 2048)
				return 128;
			return 64;
		}

		public static int[] low_primes = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97,
				101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179,
				181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269,
				271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367,
				373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461,
				463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571,
				577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661,
				673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773,
				787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883,
				887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997};

		bool is_huge_prime(BigInteger source, int mr_min_rounds)
		{
			for (int i = 0; i < low_primes.Length; i++)
			{
				if (source % low_primes[i] == 0)
				{
					return false;
				}
			}

			BigInteger r = source - BigInteger.One;
			int s = 0;

			//while (r % 2 == BigInteger.Zero)
			while (0 == (r&1))
			{
				r /= 2;
				s += 1;
			}

			byte[] bytes = new byte[source.ToByteArray().LongLength];
			BigInteger a = BigInteger.Zero;

			/* Miller–Rabin test composite detection probability: 75% (each round)
			 * i.e. test false negative probability: 4^-n per round
			 * 64 rounds --> undetected composite probability: 2^-128
			 */
			for (int i = 0; i < mr_min_rounds; i++)
			{
				while (a < 2 || a >= source - 2)
				{
					rng.GetBytes(bytes);
					a = new BigInteger(bytes);
				}

				BigInteger x = BigInteger.ModPow(a, r, source);
				if (x != BigInteger.One)
				{
					int xy = 0;
					while (x != source - 1)
					{
						if (xy == s - 1)
						{
							return false;
						}
						xy++;
						x = BigInteger.ModPow(x, 2, source);
						//if (x == BigInteger.One) return false;
						continue;
					}
				}
			}
			/* probably prime */
			return true;
		}

		BigInteger mmi(BigInteger a, BigInteger nn)
		{
			BigInteger i = nn, v = BigInteger.Zero, d = BigInteger.One;
			while (a > BigInteger.Zero)
			{
				BigInteger t = i / a, x = a;
				a = i % x;
				i = x;
				x = d;
				d = v - t * x;
				v = x;
			}
			v %= nn;
			if (v < BigInteger.Zero) v = (v + nn) % nn;
			return v;
		}

		static (string, double) rsaPadding(string message)
		{
			string s = message;
			double n = Math.Ceiling(message.Length / (double)partOfMessage.Length);
			int nn = message.Length % partOfMessage.Length;
			int chars_to_add = partOfMessage.Length - nn;
			int i = 0;
			while (i != chars_to_add)
			{
				s += " ";
				i++;
			}
			return (s, n);
		}

		private void genprimes_handler()
		{
			ManualResetEvent resetEvent = new ManualResetEvent(false);
			int toProcess = 0;
			tb_check(textBox8);
			bool is_invalid = Regex.IsMatch(textBox8.Text, @"^\D+$");
			if (is_invalid)
			{
				toProcess = numThreads;
				tb_check(textBox8);
				return;
			}

			if (threads_valid(textBox8.Text) == 1 || threads_valid(textBox8.Text) == 2)
			{
				tb_check(textBox8);
				MessageBox.Show("threads number needs to be 0 < n < 4","check your numbah");
				return;
			}
			else if (threads_valid(textBox8.Text) == 0)
			{
				bool is_whitespace = Regex.IsMatch(textBox8.Text, @"\s+$");
				if (is_whitespace)
				{
					toProcess = numThreads;
				}
				else if (textBox8.Text == "")
				{
					toProcess = numThreads;
				}
				else
				{
					custom_threads = Convert.ToInt32(textBox8.Text);
				}
			}
			if (custom_threads != 0)
			{
				numThreads = custom_threads;
			}

			/* Start workers. */
			gp_start = DateTime.Now;
			Console.WriteLine($"\n[*] spawning genprimes threads \t({gp_start})");
			Parallel.For(0, numThreads, i =>
			{
				gen_primes();
			});
			gp_finish = DateTime.Now;
			Console.WriteLine($"[*] genprimes finished.\t({gp_finish})\n");

			Console.WriteLine($"[*] p:\n{P}");
			Console.WriteLine($"[*] q:\n{Q}\n");
			textBox1.Text = Convert.ToString(P);
			textBox2.Text = Convert.ToString(Q);

			N = P * Q;
			Console.WriteLine($"[*] n:\n{N}\n");
			textBox3.Text = Convert.ToString(N);

			Console.WriteLine("[*] eulerFunction = (p - 1) * (q - 1)");
			BigInteger eulerFunction = (P - 1) * (Q - 1);
			Console.WriteLine($"[*] eulerFunction:\n{eulerFunction}\n");

			do
			{
				using (RNGCryptoServiceProvider rg = new RNGCryptoServiceProvider())
				{
					byte[] random = new byte[block_size];
					rg.GetBytes(random);
					E = new BigInteger(random);
					E = BigInteger.Abs(E);
				}
			} while (!(E > 1) || !(E < eulerFunction) || !(BigInteger.GreatestCommonDivisor(E, eulerFunction) == 1));

			Console.WriteLine($"[*] gcd(e, eulerFunction) = {BigInteger.GreatestCommonDivisor(E, eulerFunction)}");
			Console.WriteLine($"[*] e:\n{E}\n");
			textBox4.Text = Convert.ToString(E);

			Console.WriteLine($"d = e exp -1");
			D = mmi(E, eulerFunction);
			Console.WriteLine($"[*] d:\n{D}\n");
			textBox5.Text = Convert.ToString(D);

			Console.WriteLine("[*] check inverse: e * d mod eulerFunction = 1");
			Console.WriteLine($"{E} * {D} mod {eulerFunction} = {(E * D) % eulerFunction}\n");

			Console.WriteLine("***********\n* summary *\n***********");
			Console.WriteLine($"[*] p size: {P.ToString().Length} bits");
			Console.WriteLine($"[*] q size: {Q.ToString().Length} bits");
			Console.WriteLine($"[*] n size: {N.ToString().Length} bits");
			Console.WriteLine($"[*] e size: {E.ToString().Length} bits");
			Console.WriteLine($"[*] d size: {D.ToString().Length} bits");
			Console.WriteLine($"[*] tried primes: {candidates_tried}");
			Console.WriteLine($"[*] gp_start: {gp_start}");
			Console.WriteLine($"[*] gp_finish: {gp_finish}");
			Console.WriteLine($"[*] generating primes took: {gp_finish - gp_start}");
			Console.WriteLine("\n");

			MessageBox.Show("got primes y'all", "info");
		}

		private int threads_valid(string input)
		{
			int returnme = 0;
			bool is_whitespace = Regex.IsMatch(input, @"\s");
			bool is_invalid = Regex.IsMatch(input, @"\D");
			bool has_num = Regex.IsMatch(input, @"\d");
			if (input == "" || is_whitespace)
			{
				returnme = 0;
			}
			if (is_invalid)
			{
				returnme = 2;
			}
			else if (is_invalid)
			{
				if (has_num)
				{
					returnme = 1;
				}
				else if (Convert.ToInt32(input) <= 0 || Convert.ToInt32(input) > 3)
				{
					returnme = 1;
				}
			}
			else if (has_num)
			{
				if (is_invalid)
				{
					returnme = 2;
				}
				else if(Convert.ToInt32(input) <= 0 || Convert.ToInt32(input) > 3)
				{
					returnme = 1;
				}
			}

			return returnme;
		}

		void tb_invalid_input(TextBox tb)
		{
			tb.BackColor = Color.PaleVioletRed;
		}
		void tb_valid_input(TextBox tb)
		{
			tb.BackColor = Color.White;
		}
		private void tb_check(TextBox tb)
		{
			if (threads_valid(tb.Text) == 0)
			{
				tb_valid_input(tb);
				label14.Text = "";
				label14.Enabled = false;
			}
			else if (threads_valid(tb.Text) == 1)
			{
				tb_invalid_input(tb);
				label14.Enabled = true;
				label14.Text = "needs to be 0 < n < 4";
				return;
			}
			else if (threads_valid(tb.Text) == 2)
			{
				tb_invalid_input(tb);
				label14.Enabled = true;
				label14.Text = "needs to be a number";
				return;
			}
		}


		private void keysize_handler()
		{
			if (button5.Text == "1024b")
			{
				button5.Text = "2048b";
				randbytesize = b2048;
				block_size = ((Convert.ToInt32(key_size.b2048)*2)/8) - 1;
			}
			else if (button5.Text == "2048b")
			{
				button5.Text = "3072b";
				randbytesize = b3072;
				block_size = ((Convert.ToInt32(key_size.b3072)*2)/8) - 1;
				MessageBox.Show("gen_primes will likely take some time", "caution");
			}
			else if (button5.Text == "3072b")
			{
				button5.Text = "4096b";
				randbytesize = b4096;
				block_size = ((Convert.ToInt32(key_size.b4096)*2)/8) - 1;
				MessageBox.Show("gen_primes will likely take some time", "cautioncautioncautioncautioncaution");
			}
			else
			{
				button5.Text = "1024b";
				randbytesize = b1024;
				block_size = ((Convert.ToInt32(key_size.b1024)*2)/8) - 1;
			}
		}

		string[] rsa_block(string message)
		{
			(string, double) hydra = rsaPadding(message);
			string[] hydra_pieces = new string[(int)hydra.Item2];
			int counter = 0;
			for (int i = 0; i < hydra.Item2; i++)
			{
				string hydra_piece = hydra.Item1.Substring(i * partOfMessage.Length, partOfMessage.Length).Normalize();
				hydra_pieces[i] = hydra_piece.Normalize();
				counter++;
			}
			return hydra_pieces;
		}

		(BigInteger[], BigInteger[]) encrypt(string[] hydra_pieces)
		{
			BigInteger[] colosseum = new BigInteger[hydra_pieces.Length];
			var counter_coloss = 0;
			BigInteger[] floki = new BigInteger[hydra_pieces.Length];

			byte[] gladiators = new byte[hydra_pieces[0].Length * hydra_pieces.Length];
			foreach (var hydra_piece in hydra_pieces)
			{
				for (int i = 0; i < hydra_piece.Length; i++)
				{
					gladiators[i] = Convert.ToByte(Convert.ToChar(hydra_piece.Substring(i, 1)));
				}
				M = new BigInteger(gladiators);
				colosseum[counter_coloss] = M;

				C = BigInteger.ModPow(M, E, N);
				floki[counter_coloss] = C;
				counter_coloss++;
			}
			return (floki, colosseum);
		}

		BigInteger[] decrypt(BigInteger[] magic)
		{
			BigInteger[] victim = new BigInteger[magic.Length];
			int counter = 0;
			foreach (var spell in magic)
			{
				C = spell;
				BigInteger mDecrypted = BigInteger.ModPow(C, D, N);
				victim[counter] = mDecrypted;
				Console.WriteLine($"\n[*] mdecrypted bytes: {victim[counter]}");
				counter++;
			}
			return victim;
		}

		string gimme_plaintext(BigInteger[] banks_to_rob)
		{
			string output = "";
			foreach (var bank in banks_to_rob)
			{
				byte[] clerks = bank.ToByteArray();
				for (int i = 0; i < clerks.Length; i++)
				{
					output += Convert.ToString(Convert.ToChar(clerks[i]));
				}
			}
			return output;
		}

		private void loremipsum()
		{
			textBox6.Text = "Potenti nisi accumsan adipiscing. Nonummy ad, in urna id neque per arcu mi justo facilisis nascetur arcu potenti etiam elit orci. Id nisl bibendum fringilla eu, aptent. Nam netus iaculis nascetur sapien. Eni fringilla nisi, nisl. Feugiat, suspendisse erat diam. Elit netus porttitor nec proin sem quam eni. Ve nulla sem felis purus, platea et eni ultricies nascetur, curae conubia eget, fermentum diam. Ante, hymenaeos vel, sollicitudin felis in cum cras accumsan a, sagittis. Luctus risus ullamcorper in turpis ac, eros conubia ac nascetur. Nulla. Erat tellus penatibus placerat cras commodo. Vitae lorem hac litora. Tristique neque, elementum lobortis eu pede tristique sociis netus sagittis risus. Congue ante. Odio nulla morbi, nam ipsum proin, senectus eu, condimentum.Volutpat aptent pede sodales blandit ornare dignissim phasellus. Eu, ornare mus. Parturient. Non id sapien id, integer ante, sollicitudin id proin mi ipsum. Vitae potenti. Ornare conubia vel, vestibulum adipiscing ut. Quam scelerisque egestas consectetuer natoque torquent proin odio. Justo. Est non consectetuer rutrum. Natoque ipsum magna curae massa, litora lorem. Eros tristique maecenas, vitae curae egestas lectus platea ante. Sollicitudin ornare. Turpis.Amet a. Donec nisl nibh curae mattis quam pretium ante, mi amet ut eget. Nisl nisl varius, mi rutrum curae amet sociosqu porttitor sagittis ante ve sollicitudin pretium, dolor et phasellus. Odio condimentum natoque, nulla nibh posuere, lorem nibh varius id senectus iaculis id. Semper ut facilisi eni, pede per consectetuer proin nunc vel massa fringilla habitasse curae morbi. Cubilia. Vehicula ipsum risus nam curae dui malesuada, senectus, ac morbi tristique placerat. Donec sed justo nisl hymenaeos mus, venenatis blandit, aenean diam suspendisse. Felis vulputate nunc egestas arcu, id elit. Nullam amet et aenean lacus risus pretium duis bibendum luctus, tempor habitasse. Facilisis mattis lacus faucibus. Aenean iaculis vivamus arcu eu volutpat sociis. Purus sapien ullamcorper. Platea ligula lorem. Sociis enim, scelerisque sit tellus sapien ipsum parturient et, enim dis.";
		}
		private void hellofriend()
		{
			textBox6.Text = "hello friend. hello friend? that's lame. maybe I should give you a name. but that's a slippery slope. you're only in my head, we have to remember that. shit, it's actually happening. i'm talking to an imaginary person.";
		}

		private void encrypt_handler()
		{
			if (textBox6.Text == "")
			{
				Console.WriteLine("[!] got no input");
				MessageBox.Show("empty message? come on, you surely do have something nice to say", "please don't do this to me");
				return;
			}
			if (E == 0 && textBox4.Text != "")
			{
				E = BigInteger.Parse(textBox4.Text);
			}
			if (N == 0 && textBox3.Text != "")
			{
				N = BigInteger.Parse(textBox3.Text);
			}
			if (E == 0 || N == 0)
			{
				Console.WriteLine("[!] {N,E} cannot be 0.\nbailing out...");
				MessageBox.Show("{N,E} cannot be 0.\nbailing out...", "please don't do this to me");
				return;
			}

			string message = "";
			message = textBox6.Text;
			string[] do_your_magic = rsa_block(message);
			Console.WriteLine("\n[**] do your magic:");
			foreach (var s in do_your_magic)
			{
				Console.WriteLine(s);
			}
			(BigInteger[], BigInteger[]) magic_done = encrypt(do_your_magic);
			Console.WriteLine("[**] magic done:");
			foreach (var bI in magic_done.Item2)
			{
				Console.WriteLine($"\n[**] colosseum: {bI}");
			}
			textBox7.Text = "";
			textBox7.Text = String.Join(" ", magic_done.Item1.Select(integer => integer.ToString()));

			//BigInteger[] magic_undo = decrypt(magic_done.Item1);
			//Console.WriteLine("\n[**] magic undo:");
			//foreach (var Ib in magic_undo)
			//{
			//    Console.WriteLine(Ib);
			//}


			//string robbed_em_blind = gimme_plaintext(magic_undo); // breakpoint here
			//Console.WriteLine($"\n[**] magic undone: {robbed_em_blind}");


			MessageBox.Show("great success!\nalso see console for more detail", "done");
		}
		private void decrypt_handler()
		{
			if (textBox6.Text == "")
			{
				Console.WriteLine("got no input");
				MessageBox.Show("nothing to decrypt? come on...", "please don't do this to me");
				return;
			}
			if (D == 0 && textBox5.Text != "")
			{
				D = BigInteger.Parse(textBox5.Text);
			}
			if (N == 0 && textBox3.Text != "")
			{
				N = BigInteger.Parse(textBox3.Text);
			}
			if (D == 0 || N == 0)
			{
				Console.WriteLine("[!] {N,D} cannot be 0.\nbailing out...");
				MessageBox.Show("{N,D} cannot be 0.\nbailing out...", "please don't do this to me");
				return;
			}

			string[] str_magic_done = textBox6.Text.Split(' ');
			BigInteger[] magic_done = new BigInteger[str_magic_done.Length];
			for (int i = 0; i < str_magic_done.Length; i++)
			{
				magic_done[i] = BigInteger.Parse(str_magic_done[i]);
			}
			BigInteger[] magic_undo = decrypt(magic_done);
			Console.WriteLine("\n[**] magic undo:");
			foreach (var Ib in magic_undo)
			{
				Console.WriteLine(Ib);
			}

			string robbed_em_blind = gimme_plaintext(magic_undo);
			Console.WriteLine($"\n[**] magic undone: {robbed_em_blind}");
			textBox7.Text = "";
			textBox7.Text = robbed_em_blind;
			MessageBox.Show("very nice, decrypted!", "great success!");
		}

		private void cryptmagic_handler()
		{
			Console.WriteLine("[*] awaiting input");

			if (encr)
			{
				encrypt_handler();
			}
			else if (!encr)
			{
				decrypt_handler();
			}
		}



		private void button1_Click(object sender, EventArgs ev)
		{
			/* do_crypto button*/
			try
			{
				cryptmagic_handler();
			}
			catch (OverflowException oe)
			{
				Console.WriteLine(oe);
				MessageBox.Show($"overflow exception caught.\nhint: probably your message contains forbidden characters. if so, try removing diacritics.", "error");
			}
			catch(Exception e)
			{
				Console.WriteLine(e);
				MessageBox.Show($"unexpected exception caught.\n", "error");
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			/* mode button */
			enc *= (-1);
			if (enc < 0)
			{
				encr = false;
				button2.Text = "decrypting";
				textBox6.ReadOnly = true;
				textBox7.ReadOnly = false;
				label6.Text = "c";
				label7.Text = "m";
				button7.Text = "cp m c";
			}
			else if (enc > 0)
			{
				encr = true;
				button2.Text = "encrypting";
				textBox6.ReadOnly = false;
				textBox7.ReadOnly = true;
				label6.Text = "m";
				label7.Text = "c";
				button7.Text = "cp c m";
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			genprimes_handler();
		}
		private void button4_Click(object sender, EventArgs e)
		{
			loremipsum();
		}
		private void button6_Click(object sender, EventArgs e)
		{
			/* hello friend button */
			hellofriend();
		}
		private void button5_Click(object sender, EventArgs e)
		{
			/* key size button */
			keysize_handler();
		}

		private void button7_Click(object sender, EventArgs e)
		{
			/* copy for decryption button*/
			if (textBox7.Text != "")
			{
				string tmp = textBox7.Text;
				textBox7.Text = "";
				textBox6.Text = tmp;
				button2.PerformClick();
				MessageBox.Show("message copied", "great success!");
			}
			else
			{
				MessageBox.Show("no good stuff to copy here", "info");
			}
		}

		private void textBox8_TextChanged(object sender, EventArgs e)
		{
			/* threads textbox */
			tb_check(textBox8);
		}
	}
}

